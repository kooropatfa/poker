require 'socket'
require 'json'

class PokerPlayer
  def initialize(server)
    @server = server
    join
  end

  def collect_data
    value = $stdin.gets.chomp
    @server.puts value
  end

  def join
    Thread.new do
      loop {
        response = JSON.parse(@server.gets.chomp)

        puts response['msg']
        break if response['status'] == 'final_result'

        Thread.new { collect_data }
      }
    end.join
  end
end

server = TCPSocket.open("localhost", 3000)
PokerPlayer.new(server)

