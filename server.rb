require 'socket'
require_relative 'lib/poll.rb'

class PokerServer

  attr_accessor :server, :polls, :connections

  def initialize(ip, port)
    @server = TCPServer.open(ip, port)
    @polls = {}
    @connections = {}
    start
  end

  private

  def start
    Poll.new(self)
  end
end

PokerServer.new("localhost", 3000)
