class ProgressBar
  VOTE_SIZE = 5
  def initialize(votes, players_number)
    @votes = votes
    @players_number = players_number
  end

  def render
    "#{progress_bar} [#{@votes.size} out of #{@players_number} votes added"
  end

  private

  def progress_bar
    bar_size = @players_number * VOTE_SIZE
    Array.new(bar_size, ' ')
    votes_number = @votes.size * VOTE_SIZE
    last_vote_index = votes_number - 1

    bar = Array.new(bar_size, " ")
    bar[0..last_vote_index] = Array.new(votes_number, 'X')

    "[#{bar.join}]"
  end
end
