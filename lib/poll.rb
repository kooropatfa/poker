require_relative 'allowed_answers.rb'
require_relative 'responses.rb'
require_relative 'progress_bar.rb'
require_relative 'poll_result.rb'

class Poll
  include AllowedAnswers

  attr_accessor :poker_server

  def initialize(poker_server)
    @poker_server = poker_server
    @server = poker_server.server
    @votes = {}
    run_poll
  end

  private

  def run_poll
    set_poll_name
    set_number_of_participants

    loop {

      Thread.start(@server.accept) do | player |
        if poll_completed?
          player.puts Responses.poll_closed if poll_completed?
          Thread.exit
        end

        set_player(player)
        listen_to_votes(player)
        finish_the_poll(player)

        # TODO 1

        Thread.current.terminate
        Thread.current.exit
      end

      break if poll_completed?
    }

    puts Responses.final_result(message: PollResult.new(@votes).render_message)
    puts "\n\n\nThanks for choosing this software!"
  end

  def finish_the_poll(player)
    poker_server.connections[@poll_name].each do |name, user|
      user.puts Responses.final_result(message: PollResult.new(@votes).render_message)
      poker_server.connections[@poll_name].delete(name)
    end
  end

  def set_poll_name
    puts "  Please enter new poll name\n\n"

    loop {
      @poll_name = $stdin.gets.chomp

      if @poll_name.empty?
        puts "Name can't be blank"
      elsif @poll_name == 'exit'
        poker_server.stop!
      elsif poker_server.polls.keys.include?(@poll_name)
        puts 'Such poll already exists. Choose different name.'
      else
        poker_server.polls[@poll_name] = self
        poker_server.connections[@poll_name] = {}
        break
      end
    }
  end

  def set_number_of_participants
    puts "Please set number of participants\n\n"

    loop {
      @players_number = $stdin.gets.chomp

      if @players_number.empty?
        puts "Number can't be blank"
      elsif %w{0 1}.include?(@players_number) || @players_number.to_i == 0
        puts JSON.parse(Responses.invalid_number)['msg']
      else
        @players_number = @players_number.to_i
        puts "Starting poll '#{@poll_name}' poll for #{@players_number} participants!"
        break
      end
    }
  end

  def set_player(player)
    player.puts Responses.choose_name

    loop {
      name = player.gets || ""
      name.chomp!

      if name.empty?
        player.puts Responses.name_cant_be_blank
      elsif poker_server.connections[@poll_name].empty?
        add_player(player, name)
        return { player: player, name: name }
        break
      else
        # TODO 2
        poker_server.connections[@poll_name].each do |player_name, player_client|
          if name == player_name || player == player_client
            player.puts Responses.name_already_taken
          else
            add_or_reject_player(player, name)
            return { player: player, name: name }
            break
          end
        end
      end
    }
  end

  def add_or_reject_player(player, name)
    send_reject_message_to(player) if poll_full? and return

    add_player(player, name)
  end

  def add_player(player, name)
    connections = poker_server.connections[@poll_name].dup
    connections[name] = player
    poker_server.connections[@poll_name] = connections
    puts "#{name} connected!"
  end

  def send_reject_message_to(player)
    player.puts Responses.poll_full
  end

  def poll_full?
    @players_number == poker_server.connections[@poll_name].size
  end

  def poll_completed?
    @votes.size == @players_number
  end

  def listen_to_votes(player)
    player.puts Responses.please_vote

    loop {
      break if poll_completed?

      vote = player.gets || ''
      vote.chomp!
      name = poker_server.connections[@poll_name].key(player)

      if @votes.keys.include?(name)
        player.puts Responses.already_voted
      elsif !ALLOWED_ANSWERS.include?(vote)
        player.puts Responses.invalid_vote
      else
        @votes[name] = vote
        puts "#{name} submitted a vote in #{@poll_name} poll"

        poker_server.connections[@poll_name].each do |name, user|
          user.puts Responses.voting_progress(message: ProgressBar.new(@votes, @players_number).render)
        end
      end
    }
  end
end
