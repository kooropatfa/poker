require_relative 'poll.rb'
require_relative 'allowed_answers.rb'

class PollResult
  include AllowedAnswers

  def initialize(votes)
    @votes = votes
    @sorted_votes = {}.tap { |sorted| ALLOWED_ANSWERS.each { |a| sorted[a] = [] } }
    @counted_votes = {}
  end

  def render_message
    assign_participants
    count_votes

    msg = "\n\n\n\n\n\n\n     Final note: #{final_note} \n"
    msg << "--------------------\n"

    @sorted_votes.each do |vote, participants|
      msg << "#{vote} - #{participants.join(', ')} \n"
    end

    msg
  end

  private

  def assign_participants
    @votes.each { |name, vote| @sorted_votes[vote] << name }
  end

  def count_votes
    @sorted_votes.map { |vote, names| @counted_votes[vote] = names.size }
  end

  def final_note
    @counted_votes.select {|k, v| v == @counted_votes.values.max}.keys.join(' / ')
  end
end
