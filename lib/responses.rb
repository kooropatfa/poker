require_relative 'allowed_answers.rb'

class Responses
  include AllowedAnswers

  RESPONSES = {
    choose_name:        'Please choose your name.',
    name_cant_be_blank: "Name can't be blank.",
    name_already_taken: 'This name is already taken.',
    #choose_poll:        'Thanks! Now enter the poll name.',
    #no_such_poll:       'No such poll!',
    please_vote:        'Please submit your vote',
    invalid_vote:       "Please choose one of following answers: #{ALLOWED_ANSWERS.join(', ')}.",
    already_voted:      'It is not allowed to vote twice. Please wait for others.',
    invalid_number:     'It does not make sense. You need at least two participants to vote.',
    poll_full:          'This poll is already closed. Enter another name',
    poll_closed:        'This poll is already closed. Choose another poll',
    voting_progress:    nil,
    final_result:       nil
  }

  RESPONSES.each do |stat, msg|
    self.class.define_method(stat.to_s) do |message: msg|
      Serializer.serialize(status: stat,
                           message: message)
    end
  end

  class Serializer
    require 'json'

    def self.serialize(status:, message:)
      { status: status, msg: message }.to_json
    end
  end
end
